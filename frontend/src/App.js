import React from 'react';
import './App.css';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
//import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';

const tasks = [
  {
  id : 1,
  task_name: "Learn Apollo",
  status: true,
 },
 {
  id : 2,
  task_name: "Learn Apollo client",
  status: false,
 },
 {
  id : 3,
  task_name: "Learn Apllo server",
  status: false,
 }
]

function App() {
  const [listOfTasks, setlistOfTasks] = React.useState(tasks);
  const [input, setInput] = React.useState('');
  const handleAddClick = () => {
    const newTask = {
      id: 4,
      task_name: input,
      status : false
    }
    setlistOfTasks([...listOfTasks, newTask]);
    setInput('');
  };
  return (
    <div className="App">
      <h1>Todo App</h1>
      <div style={{textAlign:'center'}}>
      <div style={{display:'flex' , justifyContent: 'space-around' , width : "700px", margin:"60px auto"}}>
      <TextField
          style={{width : "50%"}}
          label="Add Task"
          variant="outlined"
          value={input}
          onChange={(e)=> setInput(e.target.value)}
        />
        <Button variant="contained" color="primary" onClick={handleAddClick}>
        Add Task
      </Button>
      </div>
      <List style={{width : '750px', display: 'flex', flexDirection : 'column', margin:"30px auto", boxShadow : '0px 4px 25px rgba(0, 0, 0, 0.15)', padding : '20px', borderRadius : '8px'}}>
              {listOfTasks.map((e) => (
                <ListItem key={e.id} style={{color : e.status ? 'green' : 'red' }}>
                  <ListItemText
                    primary={e.task_name}
                    secondary={e.status ? "Complete" : "Not Complete"}
                  />
                    <IconButton edge="end" aria-label="delete">
                      <EditIcon style={{color: 'black'}} />
                    </IconButton>
              
                  
                    <IconButton edge="end" aria-label="delete">
                      <DeleteIcon style={{color: '#ff5b00'}} />
                    </IconButton>
                
                </ListItem>
              ))}
      </List>
      </div>
    </div>
  );
}

export default App;
